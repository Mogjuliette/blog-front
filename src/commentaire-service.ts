import axios from "axios";
import { Commentaire } from "./entities";

/* export async function fetchAllCommentaires() {
    const response = await axios.get<Commentaire[]>('http://localhost:8000/api/commentaire');
    return response.data;
} */

export async function fetchAllCommentairesByArticle(id:number) {
    const response = await axios.get<Commentaire[]>('http://localhost:8000/api/commentaire/article/'+id);
    return response.data;
}

/* export async function fetchOneCommentaire(id:number) {
    const response = await axios.get<Commentaire>('http://localhost:8000/api/commentaire/'+id);
    return response.data;
}

export async function postCommentaire(commentaire:Commentaire, id_article:number) {
    const response = await axios.post<Commentaire>('http://localhost:8000/api/commentaire', commentaire);
    return response.data;
}  */