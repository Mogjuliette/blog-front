
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";

export default function Index() {
  return (
    <>
    <section className="hero">
     
    </section>
    <section className="m-5">
      <h1 className="text-center mb-5">Découvrez les meilleurs webcomics !</h1>
      <div className="row">
        <div className="col-md-6">
          <div>
            <img src="https://64.media.tumblr.com/c14eef23c733d03ffd277804fea8b554/4f7628115cf86bd2-a4/s1280x1920/e7c86b1634807add356e76e77fefb765f9aa5cb3.jpg" className="img-fluid"/>
            <p>Always Human, un webcomic par sur le site et l'appli webtoon, maintenant disponible en format papier</p>
          </div>
        </div>
        <div className="col-6">
          <h2 className="my-3">Des bandes-dessinées en ligne</h2>
          <p>Les webcomics regroupent une grande variété de bandes-dessinées disponibles en ligne. Certains auteurs publient directement sur leur propre site ou blog, tandis que des applis comme Webtoon ou Tapas sont de véritables plate-formes d'accueil et de lecture pour ces histoires.</p>
          <p>Beaucoup de ceux que je vous recommande sont en anglais, la communauté internationale anglophone étant très active. Beaucoup de webcomics reprennent les codes des mangas ou des romans graphiques pour proposer des scénarios plus moderne que la bande-dessinée traditionnelle, les mangas et les comics. On peut trouver des histoires avec plus de représentations plus variées au sein des personnages (plus de femmes, de types de corps, d'ethnies, de personnages LGBT ou handicapés).</p>
          <p>C'est aussi l'occasion de soutenir des artistes parfois méconnus, qui s'affranchissent du système d'édition traditionnelle pour être rémunérés par les plateformes comme webtoon, par du mécénat comme avec le site Patreon, ou encore en faisant ensuite de l'autoédition en format papier.</p>
          <p>Bien entendu, on trouve de tout, que ce soit en termes de style, de dessin, d'histoires, de genre ou de qualité. C'est pourquoi je vous propose sur ce blog une sélection de ceux que j'ai préférés.</p>
          <a href="http://localhost:3000/article">Découvrez toutes les séries recommandées</a>
        </div>
      </div>
    </section>
    </>
  )
}
