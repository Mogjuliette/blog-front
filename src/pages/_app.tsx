import type { AppProps } from 'next/app'
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { useEffect } from "react";
import '@/styles/globals.css'; 


export default function App({ Component, pageProps }: AppProps) {
  useEffect(() => {
    require("bootstrap/dist/js/bootstrap.bundle.min.js"); 
  }, []);
  return (
    <>
    {/* <head>
      <link rel="preconnect" href="https://fonts.googleapis.com" />
      <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin=''  />
      <link href="https://fonts.googleapis.com/css2?family=Ubuntu&display=swap" rel="stylesheet" />
    </head> */}
    <header>
          <nav className="navbar navbar-expand-lg main-nav">
            <div className="container-fluid d-flex">
              <a className="navbar-brand ms-3" href="http://localhost:3000/">Blog best webcomics
              </a>
              <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
              </button>
              <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                <ul className="navbar-nav">
                <li className="nav-item mx-3">
                    <a className="nav-link active" aria-current="page" href="http://localhost:3000">Accueil</a>
                  </li>
                  <li className="nav-item mx-3">
                    <a className="nav-link"  href="http://localhost:3000/article">Tout les articles</a>
                  </li>
                  <li className="nav-item mx-3">
                    <a className="nav-link" href="http://localhost:3000/article/add">Ajouter un article</a>
                  </li>
                </ul>
                {/* Si j'avais poussé plus loin, j'aurais fait un formulaire de recherche qui marche

                <form className="d-flex" role="search">
                  <input className="form-control mx-2" type="search" placeholder="Recherche" aria-label="Search" />
                  <button className="btn btn-outline-success mx-2" type="submit">Chercher</button>
                </form> */}
              </div>
            </div>
          </nav>
      </header>
      <div className="main-content">
      
      <Component {...pageProps} />
      </div>
      <footer>
        <nav className="navbar bg-body-tertiary justify-content-center mt-3">
            <span className="navbar-text">
              Un blog sur les meilleurs webcomics
            </span>
        </nav>
      </footer>
    </>
  );
}
