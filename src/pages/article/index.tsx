import ArticlesList from "@/components/ArticlesList";

export default function Index(){

    return(
    <div className="container-fluid">
        <div className="p-5">
        <h1 className="text-center">Toutes les séries</h1>
        <h2 className="text-center">Des article sur chaque webcomic recommandé !</h2>
        </div>
        <div className="row g-3">
            <ArticlesList />
        </div>
    </div>)
}