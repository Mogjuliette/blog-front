import { postArticle } from "@/article-service";
import CommentaireList from "@/components/CommentairesList";
import FormArticle from "@/components/FormArticle";
import { Article } from "@/entities";
import { useRouter } from "next/router";

export default function AddArticle() {
    const router = useRouter();

    async function addArticle(article:Article) {
        const added = await postArticle(article);
        router.push('/article/' + added.id);
        }

    return (
        <>
        <div className="container-fluid">
            <div className="row">
            <div className="offset-sm-2 col-sm-8 offset-md-3 col-md-6">
            <h1 className="text-center my-5">Ajouter un nouvel article</h1>
            <FormArticle onSubmit={addArticle} />
            
            </div>
            </div>
        </div>
        </>
    );
}
