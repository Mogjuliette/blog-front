import { fetchOneArticle, updateArticle } from "@/article-service";
import FormArticleUpdate from "@/components/FormArticleUpdate";
import { Article } from "@/entities";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

export default function UpdateArticlePage() {
    const router = useRouter();
    const { id } = router.query;

    const [article, setArticle] = useState<Article>();

    useEffect(() => {
        if (!id) {
            return;
        }
        fetchOneArticle(Number(id))
            .then(data => setArticle(data))
            .catch(error => {
                console.log(error);
                if(error.response.status == 404) {
                    router.push('/404');
                }
            });
    }, [id]);
    

    async function updateThisArticle(article:Article) {
        const updated = await updateArticle(article);
        router.push('/article/' + updated.id);
        }

    return(
        <>
        <div>{!article && "erreur"}
        </div>
        {article &&
        <div className="container-fluid">
            <div className="row">
            <div className="offset-sm-2 col-sm-8 offset-md-3 col-md-6">
            <h1 className="text-center my-5">Modifier l'article</h1>
            <FormArticleUpdate onSubmit={updateThisArticle} edited={article} />
            
            </div>
            </div>
        </div>}
        </>
    )
}