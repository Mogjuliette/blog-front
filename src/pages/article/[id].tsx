import { Article } from "@/entities";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { deleteArticle, fetchOneArticle } from "@/article-service";
import CommentaireList from "@/components/CommentairesList";


export default function ArticlePage() {
    const router = useRouter();
    const { id } = router.query;
    const idNumb = Number(id);
    const [article, setArticle] = useState<Article>();

    useEffect(() => {
        if (!id) {
            return;
        }
        fetchOneArticle(Number(id))
            .then(data => setArticle(data))
            .catch(error => {
                console.log(error);
                if(error.response.status == 404) {
                    router.push('/404');
                }
            });
    }, [id]);

    if(!article) {
       return <p>Loading...</p>
    }

    async function remove() {
            await deleteArticle(id);
            router.push('/article');
        }

    function update(){
        router.push('/article/update/'+id)
        console.log("fct update")
    }

    return (
        <>
        <section className="m-3 m-sm-5">
            <img src={article?.image} alt={"illustration de"+article?.titre} className="img-one-article"/>
            <h1 className="my-3">{article?.titre}</h1>
            <h2  className="my-3">{"Une série de "+article?.auteurBD}</h2>
            <div className="card-text">
                <p>
                    <span className="badge bg-primary">{article.genre}
                    </span>
                    
                </p>
                <p><span className="fw-light">{article.serieEnCours && "Série en cours"} {article.serieEnCours && 
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                    <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z"/>
                                    </svg>}{!article.serieEnCours && "Série terminée "}{!article.serieEnCours && 
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-check-lg" viewBox="0 0 16 16">
                                    <path d="M12.736 3.97a.733.733 0 0 1 1.047 0c.286.289.29.756.01 1.05L7.88 12.01a.733.733 0 0 1-1.065.02L3.217 8.384a.757.757 0 0 1 0-1.06.733.733 0 0 1 1.047 0l3.052 3.093 5.4-6.425a.247.247 0 0 1 .02-.022Z"/>
                                    </svg>}
                    </span></p>
            </div>
                                
            <p>{"Un article de "+article.auteurArticle+", publié le "+article.date.substring(0,10)}, {"vues : "+article.vues}</p>
            <p>{article?.texte}</p>

            <button type="button" className="btn btn-primary me-3  mt-3" onClick={update}>
            Modifier l'article
            </button>
            
            <button type="button" className="btn btn-outline-primary me-3 mt-3" data-bs-toggle="modal" data-bs-target="#exampleModal">
            Supprimer l'article
            </button>


            <div className="modal fade" id="exampleModal" tabIndex={-1} aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h1 className="modal-title fs-5" id="exampleModalLabel">Voulez-vous vraiment supprimer cet article ?</h1>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                        La suppression est définitive
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                        <button type="button" className="btn btn-danger" data-bs-dismiss="modal" onClick={remove}>Supprimmer l'article</button>
                    </div>
                    </div>
                </div>
            </div>
            
        <h2 className="smallh2 mt-5">Commentaires</h2>
        <CommentaireList id={idNumb} />
        </section>
        </>
    );
}
