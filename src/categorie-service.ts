import axios from "axios";
import { Categorie } from "./entities";


export async function fetchAllCategories() {
    const response = await axios.get<Categorie[]>('http://localhost:8000/api/categorie');
    return response.data;
}

export async function fetchOneCategorie(id:number) {
    const response = await axios.get<Categorie>('http://localhost:8000/api/categorie/'+id);
    return response.data;
}

export async function postCategorie(categorie:Categorie) {
    const response = await axios.post<Categorie>('http://localhost:8000/api/categorie', categorie);
    return response.data;
} 
