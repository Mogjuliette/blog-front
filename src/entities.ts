export interface Article {
    id?:number;
    titre:string;
    image:string;
    auteurBD:string;
    auteurArticle:string;
    date:string;
    texte:string;
    serieEnCours:boolean;
    genre:string;
    vues:number;
}

export interface Categorie {
    id:number;
    nom:string;
}

export interface Commentaire {
    id?:number;
    auteur:string;
    texteCom:string;
    date:string;
}