import { fetchAllArticles } from "@/article-service";
import { Article } from "@/entities"
import { useEffect, useState } from "react";

export default function ArticlesList(){

   const [articles, setArticles] = useState<Article[]>([]);

    useEffect(() => {
        fetchAllArticles().then(data => {
            setArticles(data)
        }) 
            
    }, [])

    return (
        <>
            
            <>{articles.map((item) => 
                <div key={item.id} className="col-md-4 col-lg-3 d-flex justify-content-center">
                    <div className="card card-width h-100">
                        <img src={item.image} className="card-img-top card-image" alt="..." />
                        <div className="card-body">
                            <h5 className="card-title">{item.titre}</h5>
                            <h6 className="card-subtitle mb-2 text-muted">{item.auteurBD}</h6>
                            <div className="d-flex justify-content-between">
                                <p className="card-text"><span className="badge bg-primary">{item.genre}</span></p>
                                <p className="card-text fw-light">{item.serieEnCours && "Série en cours"} {item.serieEnCours && 
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-three-dots" viewBox="0 0 16 16">
                                    <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z"/>
                                    </svg>}{!item.serieEnCours && "Série terminée "}{!item.serieEnCours && 
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-check-lg" viewBox="0 0 16 16">
                                    <path d="M12.736 3.97a.733.733 0 0 1 1.047 0c.286.289.29.756.01 1.05L7.88 12.01a.733.733 0 0 1-1.065.02L3.217 8.384a.757.757 0 0 1 0-1.06.733.733 0 0 1 1.047 0l3.052 3.093 5.4-6.425a.247.247 0 0 1 .02-.022Z"/>
                                    </svg>}
                                </p>
                            </div>
                            <p className="card-text">{item.texte.split(' ').slice(0, 20).join(' ')}</p>
                            <a href={"/article/"+item.id} className="card-link">Lire l'article</a>
                        </div>
                        </div>
                </div>)}</>
        </>
    ); 
    
}