
import { fetchAllCommentairesByArticle } from "@/commentaire-service";
import { Commentaire } from "@/entities"
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

interface Props {
    id: number;
}


export default function CommentaireList({id}:Props){
    
   const [commentaires, setCommentaires] = useState<Commentaire[]>([]); 

    useEffect(() => {
        fetchAllCommentairesByArticle(id).then(data => {
            setCommentaires(data);
            console.log(commentaires)
        }) 
            
    }, [])

    return (
    <>
        <div className="my-3">
        <ul className="list-group">
            {!commentaires[0] && <li className="list-group-item d-flex justify-content-center align-items-center">
                    
                    <div className="text-center fw-light">Cet article n'a pas encore de commentaires</div>
                </li>}
  
            {commentaires.map((com) => <li key={com.id} className="list-group-item d-flex justify-content-between align-items-start">
                    <div className="ms-2 me-auto">
                    <div className="fw-bold">{com.auteur}</div>
                    {com.texteCom}
                    </div>
                </li>)}
        
        </ul>
    </div>
    </>
    )
}