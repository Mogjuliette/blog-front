import { Article } from "@/entities";
import { FormEvent, useState } from "react";

interface Props {
    onSubmit:(article:Article) => void;
    edited?:Article;
}

export default function FormArticle({onSubmit, edited}:Props) {
    var curr = new Date();
    curr.setDate(curr.getDate() + 3);
    var today = curr.toISOString().substring(0,10);
    const [errors, setErrors] = useState('');
    const [article, setArticle] = useState<Article>(edited?edited:{
        titre:'',
        image:'',
        auteurBD:'',
        auteurArticle:'',
        date: today,
        texte:'',
        serieEnCours:true,
        genre:'',
        vues:0,
    });

    function handleChange(event: any) {
        setArticle({
            ...article,
            [event.target.name]: event.target.value
        });
    }

    async function handleSubmit(event:FormEvent) {
        event.preventDefault();
        try {
            onSubmit(article);

        } catch(error:any) {
            if(error.response.status == 400) {
                setErrors(error.response.data.detail);
            }
        }
    }

    return (
    <section className="bg-light p-2 p-sm-3 p-md-5">
        <form onSubmit={handleSubmit} >
            {errors && <p>{errors}</p>}

            <div className="mb-3 ">
                <label htmlFor="titre" className="form-label">Titre : </label>
                <div>
                    <div>
                        <input type="text" className="form-control half" id="titre" placeholder="" name="titre" value={article.titre} onChange={handleChange} required/>
                    </div>
                </div>
            </div>
            <div className="mb-3">
                <label htmlFor="image" className="form-label">Url de l'image : </label>
                <input type="text" className="form-control half" id="image" placeholder="" name="image" value={article.image} onChange={handleChange} required/>
            </div>
            <div className="mb-3">
                <label htmlFor="auteurBD" className="form-label">Auteur du webcomic : </label>
                <input type="text" className="form-control half" id="auteurBD" placeholder="" name="auteurBD" value={article.auteurBD} onChange={handleChange} required/>
            </div>
            <div className="mb-3">
                <label htmlFor="auteurArticle" className="form-label">Auteur de l'article : </label>
                <input type="text" className="form-control half" id="auteurArticle" placeholder="" name="auteurArticle" value={article.auteurArticle} onChange={handleChange} required/>
            </div>
            <div className="mb-3">
                <label htmlFor="date" className="form-label">Date de l'article : </label>
                <input type="date" className="form-control half" id="date" placeholder="" name="date" value={article.date} onChange={handleChange} required/>
            </div>
            <div className="mb-3">
                <label htmlFor="texte" className="form-label">Texte de l'article :</label>
                <textarea className="form-control textarea-height" id="texte" placeholder="" name="texte" value={article.texte} onChange={handleChange} required></textarea>
            </div>
            <p>Statut de la série : </p>
            <div className="form-check">
                <input className="form-check-input" type="radio" name="serieEnCours" id="serieEnCoursTrue" value={1} onChange={handleChange}/>
                <label className="form-check-label" htmlFor="flexRadioDefault1">
                    Série en cours
                </label>
            </div>
            <div className="form-check mb-3">
                <input className="form-check-input" type="radio" name="serieEnCours" id="serieEnCoursFalse" value={0} onChange={handleChange} checked />
                <label className="form-check-label" htmlFor="serieEnCoursFalse">
                    Série terminée
                </label>
            </div>
            <div className="mb-3">
                <label htmlFor="genre" className="form-label">Genre de la série : </label>
                <input type="text" className="form-control half" id="genre" placeholder="" name="genre" value={article.genre} onChange={handleChange} required/>
            </div>
            <button className="btn btn-primary mt-4">Publier l'article</button>
        </form>
    </section>)

}